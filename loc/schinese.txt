{
    "menu_AFR_Cus" : "自动连发自定义",
	"menu_AFR_Cus_desc" : "自定义自动连发模组的功能。",
	"menu_AFR_Cus_allow_single" : "允许可调节武器的自动连发",
	"menu_AFR_Cus_allow_single_desc" : "勾选以允许可调节武器的自动连发。"
}
