if not AFR then
  AFR = {}
  local file = io.open(SavePath .. "afr.txt", "r")
  if file then
    AFR = json.decode(file:read("*all"))
    file:close()
  end
end

local FIRE_RATE_LIMIT = 0.01

local _get_input = PlayerStandard._get_input
function PlayerStandard:_get_input(t, ...)
  local input = _get_input(self, t, ...)

  if self._equipped_unit then
    local weap_base = self._equipped_unit:base()
    if weap_base.clip_empty and weap_base:clip_empty() and (self.RUN_AND_RELOAD or not self._running) then
      if not weap_base:out_of_ammo() and not self:_is_using_bipod() then
        input.btn_reload_press = true
      end
    elseif not AFR[self._equipped_unit:base()._name_id] and input.btn_primary_attack_state and not weap_base.charge_multiplier and not self:_is_reloading() then
      if self._equipped_unit:timer():time() >= weap_base._next_fire_allowed + FIRE_RATE_LIMIT then
        if weap_base._afr_is_single == nil then
          if AFR_Cus.settings.allow_single then
            weap_base._afr_is_single = true
          else
            weap_base._afr_is_single = not tweak_data.weapon[weap_base._name_id].CAN_TOGGLE_FIREMODE and not table.contains(weap_base._blueprint, "wpn_fps_upg_i_singlefire") and true or false
          end
        end
        if weap_base._afr_is_single then
          input.btn_primary_attack_press = true
        end
      end
    end
  end

  return input
end