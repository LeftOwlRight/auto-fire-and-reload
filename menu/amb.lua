--if not AFR then
--  return
--end

dofile(ModPath .. "automenubuilder.lua")

AFR_Cus = AFR_Cus or {
  settings = {
    allow_single = true
  }
}

AutoMenuBuilder:load_settings(AFR_Cus.settings, "AFR_Cus")
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusAFR_Cus", function(menu_manager, nodes)
  
  AutoMenuBuilder:create_menu_from_table(nodes, AFR_Cus.settings, "AFR_Cus", "blt_options")
end)

local mpath=ModPath

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_AFR_Cus", function(loc)
  local lang, path=SystemInfo and SystemInfo:language(), "loc/english.txt"
    if lang==Idstring("schinese") then
      path="loc/schinese.txt"
    end
  loc:load_localization_file(mpath..path)
end)

