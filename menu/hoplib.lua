--if not AFR then
--  return
--end

AFR_Cus = {}
AFR_Cus.settings = {
    allow_single = true
}

local builder = MenuBuilder:new("AFR_Cus", AFR_Cus.settings, AFR_Cus.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusAFR_Cus", function(menu_manager, nodes)
  builder:create_menu(nodes)
end)

local mpath = ModPath

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_AFR_Cus", function(loc)
  local lang, path=SystemInfo and SystemInfo:language(), "loc/english.txt"
    if lang==Idstring("schinese") then
      path="loc/schinese.txt"
    end
  loc:load_localization_file(mpath..path)
end)

